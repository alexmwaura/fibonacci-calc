# Fibonacci calculator

## Install packages
```bash=
    npm i
```
## Start the app with 
```bash=
    node server.js
```

## This a simple web server that displays static html file for the purpose of demonstarting how web workers are used. For further clarification email ```alexmwaura43@gmail.com```