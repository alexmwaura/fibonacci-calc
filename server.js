"use strict";

var path = require("path");
var http = require("http");
var nodeStaticAlias = require("node-static-alias");

const PORT = 8049;

var httpServer = http.createServer(handleRequest);

var staticServer = new nodeStaticAlias.Server(path.join(__dirname,"web"),{
	serverInfo: "Web Workers",
	cache: 1
});


httpServer.listen(PORT);
console.log(`Server started on http://localhost:${PORT}...`);


// *******************************

async function handleRequest(req,res) {
    staticServer.serve(req,res,function onStaticComplete(err){
        if (err) {
            res.writeHead(404);
            res.end();
        }
    });
}
